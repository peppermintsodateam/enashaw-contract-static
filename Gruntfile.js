module.exports = function(grunt) {
	grunt.initConfig({

		less:{
			dev:{
				options:{
					paths: ["./less"],
                    yuicompress: true

				},

				files: {
      				"src/css/main.css": "src/less/style.less"
    			}
			}
		},


		cssmin: {
			prod:{
				files: {
					"build/css/main.css" : "src/css/main.css"
					/*"build/css/magnific-popup.css" : "src/css/magnific-popup.css"*/
				}
			}
		},

		uglify: {

			prod:{
				options:{
					mangle:false//aby zachowac nazwy zmiennych
				},

				files:{
					"build/js/main.js": "src/js/main.js",
					"build/js/case-studies.js": "src/js/case-studies.js",
					"build/js/vendor/ajax-load.js": "src/js/vendor/ajax-load.js",
					"build/js/vendor/google-map.js": "src/js/vendor/google-map.js"
					
				}
			}	
		},

		autoprefixer: {

			dev: {
				options: {
					browsers:["last 25 version"]
				},

				src:"src/css/*.css"
			}

		},

		
		/*concat: {

			options: {
				separator: ';',
			},

			prod: {
				src: ['src/js/slick.min.js', 'src/js/jquery.counterup.js', 'src/js/jquery.magnific-popup.min.js'],
				dest: 'src/js/plugins.js'
			}
		},*/

		imagemin:{
			options:{
				optimizationLevel: 3
			},

			prod:{
				files:[
					{
						expand:true,
						cwd:"src/gfx/",
						src:"*",
						dest:"build/gfx"
					}
				]
			}
		},


		concat_css: {
		    options: {
		      // Task-specific options go here. 
		    },

		    prod: {
		      src: ["src/css/owl.theme.default.min.css", "src/css/owl.carousel.min.css","src/css/magnific-popup.css"],
		      dest: "src/css/plugins.css"
		    },
  		},

		watch:{
			options:{
				livereload:true
			},

			dev:{
				files:["src/**/*"],
				tasks:["dev"] 
			}
		}

	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-concat-css');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');

	//"concat","concat_css" - add later to []

	grunt.registerTask("dev",["less", "autoprefixer"]);
	grunt.registerTask("prod",["uglify","cssmin","concat_css","imagemin"]);//server produkcyjny
	grunt.registerTask("default","dev");//po wpisaniu grunt, wykonaja si tylko wersja developerska
	grunt.registerTask("build",["dev","prod"]);
}