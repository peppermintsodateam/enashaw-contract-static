(function($){

	// Global variables
	var caseRotator = $('#cases-slider');
	var slidesContainer = caseRotator.find('.case-container');
	var allCases = slidesContainer.children();
	var slides = $('.slide-case, .case-bck-container');
	var slidesCount = allCases.length;
	var sections = $('.slide-case');
	var refresh = false;
	var loadedPage = true;
	var wasUsedBackButton = false;
	var countSlides = 0;
	var currentSlideID = 0;
	var animationStart = false;

	var paginationContainer = $('.pags-container');
	var paginationList = $('#pagination-case');

	var navLeft = $('.footer-arrows').find('.arrow-inner-left');
	var navRight = $('.footer-arrows').find('.arrow-inner-right');

	var preloader = $('#preloader');
	var imagesToLoad = $('.bcg').length;
	var loadingProgress = 0; //timeline progress - starts at 0
	var loadedCount = 0; //current number of images loaded


	function loadComplete() {

		// preloader out
		var preloaderOutTl = new TimelineMax();
		var caseLeft = $('.case-content-inner').find('.case-left');
		var buttonTitle = $('.case-content-inner').find('.btn-column');
		var caseRight = $('.case-content-inner').find('.case-right');
		var shapeNavigation = $('.shape-triangle');

		preloaderOutTl
			//.to($('.progress'), 0.3, {autoAlpha: 0, ease:Back.easeIn})
			//.to($('.txt-perc'), 0.3, {autoAlpha: 0, ease:Back.easeIn}, 0.1)

			.to($('.intro-logo-container'),0.5, {autoAlpha: 0, ease:Back.easeIn})
			.set($('body'), {className: '-=is-loading'})
			.to($('#preloader'), 0.7, {autoAlpha: 0, ease:Power4.easeInOut})
			.set($('#preloader'), {className: '+=is-hidden'})

			.to(shapeNavigation,0.2,

				{
					autoAlpha:1,
					left:0,
					ease:Power4.easeInOut
				}

			)

			.to(buttonTitle,0.5,

				{
					autoAlpha:0,
					x:10,
					ease:Power4.easeInOut
				}

			)

			.to(caseLeft,1.2,

				{
					scaleX: 1,
	                autoAlpha:1,
	                ease: Quint.easeOut,
	                transformOrigin:"100% 0",
	                onComplete:function() {
	                	TweenMax.to(caseLeft,0.8,

			                {
			                    scaleX: 0,
			                    autoAlpha:0,
			                    ease: Quint.easeOut,
			                    transformOrigin:"100% 0",
			                    delay:5.0,
			                    onComplete:function() {
			                    	TweenMax.to(buttonTitle, 0.5,

				                    	{
				                    		autoAlpha:1,
											x:0,
											ease:Power4.easeInOut,
											onComplete:showPanel
				                    	}

			                    	)
			                    }
			                }
            			)
	                }
				}

			)

			//return preloaderOutTl;

			if($(window).width() > 1000) {
				preloaderOutTl;
			} else {
				TweenMax.killTweensOf($('.case-left'));
			}

			
			$(window).on('resize',function(){
				setTimeout(function(){

					if($(window).width() > 1000) {
						
						console.log('Play again');

						preloaderOutTl
							.to($('.progress'), 0.3, {autoAlpha: 0, ease:Back.easeIn})
							.to($('.txt-perc'), 0.3, {autoAlpha: 0, ease:Back.easeIn}, 0.1)
							.set($('body'), {className: '-=is-loading'})
							.to($('#preloader'), 0.7, {autoAlpha: 0, ease:Power4.easeInOut})
							.set($('#preloader'), {className: '+=is-hidden'})

							.to(shapeNavigation,0.2,

								{
									autoAlpha:1,
									left:0,
									ease:Power4.easeInOut
								}

							)

							.to(buttonTitle,0.5,

								{
									autoAlpha:0,
									x:10,
									ease:Power4.easeInOut
								}

							)

							.to(caseLeft,1.2,

								{
									scaleX: 1,
					                autoAlpha:1,
					                ease: Quint.easeOut,
					                transformOrigin:"100% 0",
					                onComplete:function() {
					                	TweenMax.to(caseLeft,0.8,

							                {
							                    scaleX: 0,
							                    autoAlpha:0,
							                    ease: Quint.easeOut,
							                    transformOrigin:"100% 0",
							                    delay:5.0,
							                    onComplete:function() {
							                    	TweenMax.to(buttonTitle, 0.5,

								                    	{
								                    		autoAlpha:1,
															x:0,
															ease:Power4.easeInOut,
															onComplete:showPanel
								                    	}

							                    	)
							                    }
							                }
				            			)
					                }
								}

							)
					
					}else {
						TweenMax.killTweensOf($('.case-left'));
						console.log('Tweens killed');
					}

				},100)
			});
	}

	function showPanel() {
		var caseRight = $('.case-content-inner').find('.case-right');
		var caseLeft = $('.case-content-inner').find('.case-left');
		var buttonTitle = $('.case-content-inner').find('.btn-column');
		var t = new TimelineMax({paused:false});

		caseRight.on('mouseenter',function(){

				 t.to(caseLeft,0.5,

					{
						scaleX: 1,
						autoAlpha:1,
						ease: Quad.easeOut,
						transformOrigin:"100% 0",
					}
				)


				t.to(buttonTitle,0.5,

					{
						autoAlpha:0,
						x:10,
						ease:Power4.easeInOut
					}

				)


			}).on('mouseleave',function(){

				var t = new TimelineMax({paused:false});

				t.to(caseLeft,0.5,

					{
						scaleX: 0,
						autoAlpha:0,
						ease: Quad.easeOut,
						transformOrigin:"100% 0",
					}
				)

				t.to(buttonTitle,0.5,

					{
						autoAlpha:1,
						x:0,
						ease:Power4.easeInOut
					}

				)

			});
		}

	


	sections.filter(':first').addClass('active-slide');

	function addDataToSlides() {
		sections.each(function(i,obj){
			var that = $(this);
			that.attr('data-number', (i));
			//that.attr('data-number', (i+1)); - would increase by one so index would start from 1,2,3, etc.
		});
	}


	function createPaginationList() {

		if(!paginationList.length) {
			paginationList = $('<ul></ul>', {

				'class': 'pagination-case',
				'id':'pagination-case'

			}).appendTo(paginationContainer);
		}
	}

	function addPagElements() {
		var bulletCount = 0;

		for(var i = 0; i < slidesCount; i++) {

			var bullets = $('<li></li>', {
				'class':"bullet"
			}).appendTo(paginationList);

			var drops = $('<span></span>', {
				'class':'diamond-number'
			}).appendTo(bullets);

			drops.each(function(){

				var that = $(this);
				that.attr('data-number', bulletCount);
				bulletCount = bulletCount + 1;
				that.html("<div class='bulletNumber'>" + bulletCount + "</div>");

			});
		}

		var firstPagiBullet = $('#pagination-case li').first();
		firstPagiBullet.addClass('activeBullet');
	}

	// Set width for slides container
	function slidesContainerWidth() {
		slidesContainer.css({
			'width': (slidesCount * 100 + '%')
		});

		slides.width($(window).width());
		slides.height($(window).height());
	}

	sliderRotate = function () {

		var that = $(this);
		var targetData = that.data('number');

		var bigBck = $('.big-bck-wrapper').find('.top-bck');
		var firstBigSlide = bigBck.filter(':first').addClass('active-big');
		var thumbs = $('.all-thumb-wrapper').find('.thumb-bck');
		var thumbsWrappers = $('.all-thumb-wrapper');


		$('#pagination-case li').removeClass('activeBullet');
		that.parent('li').addClass('activeBullet');

		var target = $('#cases-slider').find('.slide-case').filter(function(){
			return $(this).data('number') === targetData;
		});

		var targetWidth = target.width();
		var targetIndex = target.index();
		var targetMargin = -(targetIndex * targetWidth);

		var currentSlide = target.data('number');

		//Akualizowanie zmiennej currentSlideID - zapobiegnie podwojnej potrzeby
			//klika w next prev, po "szybkim" przejsciu z klika paginacji do prev i next
			currentSlideID = currentSlide;

		if(currentSlideID === 0) {
				switchArrowsBtn(false,true);
	
			}else if (currentSlide === slidesCount - 1) {
				switchArrowsBtn(true,false);
			} else {
				switchArrowsBtn(true,true);
		}



			if(!refresh) {
				var time = (loadedPage) ? 1 : 0.8;
				loadedPage = false;
				sections.removeClass('active-slide');



				animationStart = true;

				 TweenMax.to(slidesContainer, time, 
					 {
					 	"margin-left":targetMargin,
		              	 ease: Power4.easeInOut,

		              	 onComplete:function() {
		              	 	var currentContainer = sections.eq(currentSlide);

		              	 	currentContainer.addClass('active-slide');
		              	 	thumbsWrappers.eq(currentSlide).find('.thumb-bck').first().click();
		              	 	
		              	 	var currentIndexSection = $('.slide-case.active-slide').index() + 1;
		              	 	 $('.bullet-text').html(''+currentIndexSection+'/'+slidesCount+'');

		              	 	 // Be aware! - needs to be added to swipe smoothly
		              	 	 animationStart = false;
		              	 }
					 }
				 )

			}else {
				$('.case-container').css('margin-left', targetMargin);
			}

			
			refresh = false;
		}

		function addNumbers() {

			var currentIndexSection = $('.slide-case.active-slide').index() + 1;
			$('.bullet-text').html(''+currentIndexSection+'/'+slidesCount+'');
		}



	// Main navigation functinality
	runMainNav = function() {
		var that = $(this);
		var nav = $('.main-nav-wrapper');
		var background = nav.find('.background');
		var container = nav.find('.nav-container');
		var logo = $('.shape-triangle');
		var mainNavigationWrapper = $('.navigation-hidden-switcher');
		var items = nav.find('.menu > .menu-el');

		var wait = nav.data('wait');

        if (wait)
            return;
        nav.data('wait', true);
		
		

		if(nav.hasClass('is-opened')) {
			that.removeClass('open-nav');
		
			var tl = new TimelineMax();
			tl.pause();

			tl.fromTo(background,1.4, {
				alpha:1,
				x: 0,
				skewX : 0,
				scaleY: 1
			}, {
				alpha: 1,
				x: -$(window).width() - logo.width(),
				skewX: -45,
				scaleY: 2,
				ease: Power2.easeInOut,
				delay:0.6
			},0);

			tl.staggerFromTo(items,0.4, {
				alpha:1,
				x:0,
			}, {

				 alpha: 0

			}, 0.1, 0);
			
			tl.call(function(){

				background.css({
					opacity:'',
					transform:''
				});

				 items.css({
                    opacity: '',
                    transform: ''
                });

				
				nav.removeClass('is-opened');
				nav.data('wait', false);
				mainNavigationWrapper.removeClass('top-index');

			});

			tl.play();


		}else {
			
			mainNavigationWrapper.addClass('top-index');
			that.addClass('open-nav');

			background.css({
				opacity:0
			});

			container.css({
				display:"block"
			});

			items.css({
                opacity: 0
            });

			var tl = new TimelineMax();
			tl.pause();
			tl.fromTo(background, 1, {
				 alpha: 1,
				 x: -$(window).width() - logo.width(),
				  skewX: -45,
				  scaleY: 2
			},{
				alpha: 1,
				x: 0,
				skewX: 0,
				scaleY: 1,
				ease: Power3.easeInOut
			}, 0);

			tl.staggerFromTo(items, 1.2, {
				alpha: 0,
                x: -80,
                delay:0.4
			}, {

				 alpha: 1,
                 x: 0,
                ease: Power4.easeInOut

			}, 0.1, 0);
			
			tl.call(function() {

			 	 background.css({
                    opacity: '',
                    transform: ''
                });

			 	  items.css({
                    opacity: '',
                    transform: ''
                });

			 	  container.css({
                    display: ''
                });

			 	
			 	nav.addClass('is-opened');
			 	
			 	nav.data('wait', false);

			 });

			tl.play();

		}
	}


	function showSlides() {
		var bigBck = $('.big-bck-wrapper').find('.top-bck');
		var firstBigSlide = bigBck.filter(':first').addClass('active-big');
		var thumbs = $('.all-thumb-wrapper').find('.thumb-bck');

		thumbs.each(function(i){

			var that = $(this);
			that.attr('data-thumbs',(i+1));

		});

		bigBck.each(function(i){

			var that = $(this);
			that.attr('data-thumbs',(i+1));

		});

		thumbs.on('click', function(){

			var that = $(this);
			$('.big-bck-wrapper').find('.top-bck').removeClass('active-big');

			var targetData = that.data('thumbs');
			var target = $('.big-bck-wrapper').find('.top-bck').filter(function(){
				return $(this).data('thumbs') === targetData;
			});

			target.addClass('active-big');


		});
	}

	//Domyslnie po załadowanou przycisk PREV ma klase "disabled" NEXT jest aktywny

	switchArrowsBtn(false,true);

    //Prev next arrows
	function switchArrowsBtn(prev,next) {

		if(next) {
			//Jezeli jestesmy na przycisku next, zrob go aktywnym
			navRight.off().on("click", gotoNextSlide).removeClass('disabled');
		} else {
			//W przeciwnym przypadku wylacz klika na next
			navRight.off().addClass('disabled');
		}

		if(prev) {
			navLeft.off().on("click", gotoPrevSlide).removeClass('disabled');
		} else {
			navLeft.off().addClass('disabled');
		}
	}

	function gotoPrevSlide() {
		var slideToGo = currentSlideID - 1;

		//console.log('slideToGo',slideToGo);


		if(slideToGo === 0) {
			//switchArrowsBtn(false,true);
		}else {
			//switchArrowsBtn(true,true);
		}

		gotoSlide(slideToGo);
	}

	function gotoNextSlide() {
		//Jezeli jest w punkcie gdzie doszło do maksymalnej ilości slajdów, wróć do pierwszego
		var slideToGo = currentSlideID + 1;
		
		if(slideToGo >= slidesCount - 1) {
			switchArrowsBtn(true,false);
			
		}else {
			switchArrowsBtn(true,true);
		}

		gotoSlide(slideToGo);
	}

	function gotoSlide(slideToGo) {
		currentSlideID = slideToGo;
		var linksPag = $('.pagination-case li').children('.diamond-number');
		linksPag.eq(currentSlideID).click();
	}

	if (!Modernizr.touch) {
		//Zdarzenia onMouseWheel
			if (document.addEventListener) {
			    document.addEventListener("mousewheel", MouseWheelHandler(), false);
			    document.addEventListener("DOMMouseScroll", MouseWheelHandler(), false);
			} else {
			    sq.attachEvent("onmousewheel", MouseWheelHandler());
			}

			function MouseWheelHandler() {

			    return function (e) {
			        // cross-browser wheel delta
			        var e = window.event || e;
			        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));


			        if(animationStart) {
			        	return false;
			        }

			        
			        if (delta > 0) {
			           	 navLeft.trigger('click');
			        }

			        //scrolling up?
			        else {
			        	
			        	navRight.trigger('click');
			        	
			             
			        }
			        return false;
			    }
			}
	}


	$("body").keydown(function(e) {
		  if(e.keyCode == 37) { // left
		    navLeft.trigger('click');
		  }
		  
		  else if(e.keyCode == 39) { // right
		     navRight.trigger('click');
		  }
		});


	function scrollToContent() {
		var arrowDown = $('.scroll-down-arrow').find('.arrow-down-inner');
		var TOP = 60;

		var contentText = $('.case-column').find('.case-text-wrapper');

		contentText.each(function(i){

			var that = $(this);
			that.attr('data-text',(i+1));

		});

		arrowDown.each(function(i){

			var that = $(this);
			that.attr('data-text',(i+1));

		});

		arrowDown.on('click', function(){
			var that = $(this);
			var targetData = that.data('text');

			var target = $('.case-column').find('.case-text-wrapper').filter(function(){
				return $(this).data('text') === targetData;
			});

			var offset = target.offset().top - TOP;

			$('html, body').stop(true,true).animate({
				scrollTop:offset
			},600)
			

		});
	}




	 // Animation on click main navigation
	 var viewportQuery = window.matchMedia('(min-width:1000px)');
      viewportQuery.addListener(menuClick);

      function menuClick(viewportQuery) {
      	 if (viewportQuery.matches) {

      	 	var mainNavigationLinks = $('#menu-main li').find('a');

      	 		mainNavigationLinks.on('click', function(event){
	      	 		event.preventDefault();
	                if (event.target !== this) return;

	                var that = $(this);
					var clickedLink = that.attr('href');
					var nav = $('.main-nav-wrapper');
					var logo = $('.shape-triangle');
					var items = nav.find('.menu > .menu-el')

					var triangleShape = $('.shape-triangle');

					TweenMax.staggerTo(items, 0.5, 

						{	opacity:0, 
							y:-100, 
							ease:Back.easeIn,
							onComplete:function() {

								TweenMax.to(triangleShape, 0.5,

								 	{
								 		left: -600,
								 		autoAlpha:0,
								 		ease: Sine. easeOut,
								 		 onComplete:function() {
								 		 	TweenMax.to($('body'), 0.7,
			                                    {
			                                        autoAlpha:0, 
			                                        ease:Power4.easeInOut,
			                                        onComplete:function() {
			                                            window.location = clickedLink;
			                                        }
			                                    }
			                                )
								 		 }
								 	}
					 			)

							}
						}, 

					0.1);

      	 		});



      	 }else {

  	 		TweenMax.to($('body'),0.7,
                {
                    autoAlpha:1, 
                    ease:Power4.easeInOut
                }
            )

      	 }
      }


      var switchQuery = window.matchMedia('(min-width: 1000px)');
      switchQuery.addListener(logoClick);

      function logoClick(switchQuery) {

      		 if (switchQuery.matches) {
      		 	var logoLink = $('.logo-link-wrapper').find('.logo-link');

      		 	logoLink.on('click',function(e){

      	 			e.preventDefault();

      	 			var that = $(this);
				    var clickedLink = that.attr('href');
				    var triangleShape = $('.shape-triangle');

      				TweenMax.to(triangleShape, 0.5,

      				 	{
      				 		ease:Power4.easeInOut,
      				 		left:-300,
      				 		autoAlpha:0,
      				 		 onComplete:function() {
      				 		 	TweenMax.to($('body'), 0.7,
                                      {
                                          autoAlpha:0, 
                                          ease:Power4.easeInOut,
                                          onComplete:function() {
                                              window.location = clickedLink;
                                          }
                                      }
                                  )
      				 		 }
      				 	}
      				)
      	 	});


      		 }else {
      		 	TweenMax.to($('body'),0.7,
	                {
	                    autoAlpha:1, 
	                    ease:Power4.easeInOut
	                }
            	)
      		 }

      }


	$(function(){
		loadComplete();
		// Calling navigation function
		$('.nav-toggle').off('click').on('click', runMainNav);

		slidesContainerWidth();
		addDataToSlides();
		createPaginationList();
		addPagElements();
		addNumbers();
		showSlides();
		menuClick(viewportQuery);
		logoClick(switchQuery);
		scrollToContent();


		$('#pagination-case li').find('span').on('click', sliderRotate);
	});


	function onPopStateHandler(event) {
        var state = event.state;
        if (state) {
            wasUsedBackButton = true;

            var nav = $('.main-nav-wrapper');
			var logo = $('.shape-triangle');
			var items = nav.find('.menu > .menu-el')
			var triangleShape = $('.shape-triangle');

					TweenMax.staggerTo(items, 0.5, 

						{	opacity:1, 
							y:0, 
							ease:Back.easeIn,
							onComplete:function() {

								TweenMax.to(triangleShape, 0.5,

								 	{
								 		ease:Power4.easeInOut,
								 		left:0,
								 		autoAlpha:1,
								 		 onComplete:function() {
								 		 	TweenMax.to($('body'), 0.7,
			                                    {
			                                        autoAlpha:1, 
			                                        ease:Power4.easeInOut
			                                    }
			                                )
								 		 }
								 	}
					 			)

							}
						});

            TweenMax.to($('body'),0.4,
                {
                   autoAlpha:1,
                    ease: SlowMo.easeIn
                }
            )
        }
    }


    $(window).unload(function(){
        TweenMax.to($('body'),0.4,
            {
                autoAlpha:1,
                ease: SlowMo.easeIn
            }
        )
    });


     function changeTopHeader() {
    	var logoTriangle = $('.shape-triangle');

    	if($(window).scrollTop() > 5) {
    		logoTriangle.addClass('scroll-header');
    	}else {
    		logoTriangle.removeClass('scroll-header');
    	}
    }


	$(window).on('scroll', function(){
		changeTopHeader();
	});


	$(window).on('load',function(){

		setTimeout(function () {
            //window.onpopstate = onPopStateHandler;
        }, 1000);

		setTimeout(function () {
            
            //findHash();
        }, 1);

	});


	$(window).on('resize',function(){
		slidesContainerWidth();
		var allDiamonds = $('#pagination-case li');

		var clickedBullet = allDiamonds.filter('.activeBullet');
		clickedBullet.find('span').trigger('click');

		setTimeout(function(){


		},100);
	});


})(jQuery);