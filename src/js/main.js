(function($){

	// Global variables
	var homeRotator = $('#move-slider');
	var slidesContainer = homeRotator.find('.slides-container');
	var allSlides = slidesContainer.children();
	var slidesCount = allSlides.length;
	var slides = $('.slide, .slide-bck');
	var sections = $('.slide');
	var pagHome = $('#home-pagination');
	var paginationDiamonds = $('#home-pagination li').find('span');
	var refresh = false;
	var loadedPage = true; // After page is loaded is zero
	
	var wasUsedBackButton = false;
	var dataSlider = ['home','hospitality','housing','education', 'care', 'manufacturing'];
	var countSlides = 0;
	var navLeft = $('#move-slider').find('.arrow-left');
	var navRight = $('#move-slider').find('.arrow-right');
	var currentSlideID = 0;
	var animationStart = false;
	var controller = new ScrollMagic.Controller();

	var menuOpen = false;


	var preloader = $('#preloader');
	var imagesToLoad = $('.bcg').length;
	var loadingProgress = 0; //timeline progress - starts at 0
	var loadedCount = 0; //current number of images loaded
	var logoTriangle = $('.shape-triangle');

		


	function loadComplete() {

		// preloader out
		var preloaderOutTl = new TimelineMax();
		var shapeNavigation = $('.shape-triangle');

		preloaderOutTl
			//.to($('.progress'), 0.3, {autoAlpha: 0, ease:Back.easeIn})
			//.to($('.txt-perc'), 0.3, {autoAlpha: 0, ease:Back.easeIn}, 0.1)

			.to($('.intro-logo-container'),0.5, {autoAlpha: 0, ease:Back.easeIn})
			.set($('body'), {className: '-=is-loading'})
		
			.to($('#preloader'), 0.7, {autoAlpha: 0, ease:Power4.easeInOut})
			.set($('#preloader'), {className: '+=is-hidden'})
			

			.to(shapeNavigation,0.2,

				{
					autoAlpha:1,
					left:0,
					ease:Power4.easeInOut
				}

			)

			.to($('.js-videoPoster'),0.6,

				{
					autoAlpha:0,
					height:0,
					ease:Power4.easeInOut
				}

			)

			return preloaderOutTl;
	}


//Video settings
function videoFullHd() {
		//var allVideos = $("iframe[src^='http://www.youtube.com']");
		var videoContainer = $('.video-container');
		var fluidEl = $(window);
		var newWidth = fluidEl.width();
		
		
		videoContainer.each(function(){
			var that = $(this);

			that.width(newWidth);
			that.css('height', $(window).height() - $('.footer-page-logo').outerHeight());

	});
}


/*function videoPlay(wrapper) {
		var iframe = wrapper.find('.js-videoIframe');
		var src = iframe.attr('href'); 
		wrapper.addClass('videoWrapperActive');
		iframe.attr('src',src);
	}


	// Play the targeted video (and hide the poster frame)
	var wrapper = $('.js-videoWrapper');
	var iframe = wrapper.find('.js-videoIframe');
	var playButton = $("#play-pause");


	function playPauseVideo() {
		var video = $('#video-catax').get(0);
		var playButton = $("#play-pause");

		playButton.on('click', function(){
			$("#ena-iframe")[0].src += "&autoplay=1";
			videoPlay(wrapper);

			
		});
	}


	$(document).on('click', '.js-videoPoster', function(e){
		e.preventDefault();
		var poster = $(this);
		var wrapper = poster.closest('.js-videoWrapper');

		// Calling function written below
		videoPlay(wrapper);
	});


	playPauseVideo();*/
	

 //SETTINGS FOR MAIN SLIDER
	// Set width for slides container
	function slidesContainerWidth() {
		slidesContainer.css({
			'width': (slidesCount * 100 + '%')
		});

		slides.width($(window).width());
		slides.height($(window).height());
	}

	function addDataToSlide() {
		sections.each(function(i){

			var that = $(this);
			that.attr('data-number', (i));

		});

		sections.each(function(index,obj){
			$(obj).attr('data-slider',dataSlider[countSlides]);
			countSlides ++;
		});
	}


	function createBottomPag() {
		var dataSlider = ['home','hospitality','housing','education', 'care', 'manufacturing'];
		var countSlides = 0;

		paginationDiamonds.each(function(i){
			var that = $(this);
			that.attr('data-number', (i));
		});

		paginationDiamonds.each(function(){

			var that = $(this);
			that.attr('data-slider', dataSlider[countSlides]);
			countSlides ++;

		});


	}

	
	function addPagBullets() {

		//Adding active class to diaminds
		var firstDiamond = $('.diamonds').children().first();
		firstDiamond.addClass('active-diamond');
	}





	// Moving slides on click
	sliderRotate = function () {

		//var diamondClick = $('#home-pagination li').children();

		var that = $(this);
		var targetData = that.data('slider');

		//if (!targetData) return;

		$('#home-pagination li').children().removeClass('active-diamond');
		that.addClass('active-diamond');

		var target = $('#move-slider').find('.slide').filter(function(){
			return $(this).data('slider') === targetData;
		});


		var targetWidth = target.width();
		var targetIndex = target.index();
		var targetMargin = -(targetIndex * targetWidth);
		var currentSlide = target.data('slider');
		var currentNumber = target.data('number');


		//Akualizowanie zmiennej currentSlideID - zapobiegnie podwojnej potrzeby
		//klika w next prev, po "szybkim" przejsciu z klika paginacji do prev i next
		currentSlideID = currentNumber;


		if(currentSlideID === 0) {
				switchArrowsBtn(false,true);
	
			}else if (currentSlide === slidesCount - 1) {
				switchArrowsBtn(true,false);
			} else {
				switchArrowsBtn(true,true);
			}


		if(!refresh) {
			var time = (loadedPage) ? 1 : 0.8;
			loadedPage = false;
            sections.removeClass('active-slide');

             animationStart = true;

              TweenMax.to(slidesContainer, time, 

	              {
	              	"margin-left":targetMargin,
	              	 ease: Power4.easeInOut,

	              	onComplete:function(){

	              		var currentContainer = sections.eq(currentNumber);
						currentContainer.addClass('active-slide');

						var currentIndexSection = $('.slide.active-slide').index() + 1;
						var indexSection = $('.slide.active-slide').index();
						//var textLayer = $('.text-layer').eq(indexSection);

						var textLayer = $('.slide-key-message').eq(indexSection);

						
						// Remove animation of text layer on each slide change
						TweenMax.to($('.slide-key-message'), 1.2,
                            {
                                x:-10,
                                autoAlpha:0,
                                ease: Power4. easeOut
                            }
                        )


                        TweenMax.to(textLayer, 1.2,
                            {
                                x:0,
                                autoAlpha:1,
                                ease: Power4. easeOut
                            }
                        )



						animationStart = false;
	              	}

	              }

              )
		} else {
			$('.slides-container').css('margin-left', targetMargin);
		}

		refresh = false;

		if (typeof history.pushState !== "undefined" && !wasUsedBackButton) {
            // Zmiana url w pasku przegladarki
            var id = target.data('slider');
            history.pushState({id: id}, '', "#" + id);
            wasUsedBackButton = false;
        }

         wasUsedBackButton = false;
	}


	//Intro for text content animation when page is loaded
	function introSlide() {
		var index = sections.index();
		var firstSection = sections.eq(index);
		var currentTextSection = firstSection.find('.slide-key-message');

		TweenMax.to(currentTextSection, 1.2,
			{
				x:0,
				autoAlpha:1,
				delay:0.2,
				ease: Power4. easeOut
			}
		)
	}


	function findHash() {

        // Replace usuwa has z data 
        var hash = (window.location.hash).replace('#', "");
        var paginationDiamonds = $('#home-pagination li').children();
        $('#home-pagination li').children().filter(function () {
            return $(this).data('slider') === hash;
            // Trigger click robi klika na paginacji i przesuwa po załadowaniu strony ze sliderem do odpowiedniego slajdu
        }).trigger("click");

        // jesli nie ma wybranego hasha to zrob click na pierwszym elemencie
        if (hash === '') {
            if(paginationDiamonds.length!==0) {
                $(paginationDiamonds)[0].click();
            }
        }
    }

// NEXT and PREV arrows
	//Domyslnie po załadowanou przycisk PREV ma klase "disabled" NEXT jest aktywny
	switchArrowsBtn(false,true);

    //Prev next arrows
	function switchArrowsBtn(prev,next) {

		if(next) {
			//Jezeli jestesmy na przycisku next, zrob go aktywnym
			navRight.off().on("click", gotoNextSlide).removeClass('disabled');
		} else {
			//W przeciwnym przypadku wylacz klika na next
			navRight.off().addClass('disabled');
		}

		if(prev) {
			navLeft.off().on("click", gotoPrevSlide).removeClass('disabled');
		} else {
			navLeft.off().addClass('disabled');
		}
	}


	function gotoPrevSlide() {
		var slideToGo = currentSlideID - 1;

		if(slideToGo === 0) {
			//switchArrowsBtn(false,true);
		}else {
			//switchArrowsBtn(true,true);
		}

		gotoSlide(slideToGo);
	}


	function gotoNextSlide() {
		//Jezeli jest w punkcie gdzie doszło do maksymalnej ilości slajdów, wróć do pierwszego
		var slideToGo = currentSlideID + 1;
		
		if(slideToGo >= slidesCount - 1) {
			switchArrowsBtn(true,false);
			
		}else {
			switchArrowsBtn(true,true);
		}

		gotoSlide(slideToGo);
	}


	function gotoSlide(slideToGo) {
		currentSlideID = slideToGo;
		var linksPag = $('#home-pagination li').children('.diamond-number');
		linksPag.eq(currentSlideID).click();

	}

	// Main navigation functinality

	runMainNav = function() {
		var that = $(this);
		var nav = $('.main-nav-wrapper');
		var background = nav.find('.background');
		var container = nav.find('.nav-container');
		var logo = $('.shape-triangle');
		var mainNavigationWrapper = $('.navigation-hidden-switcher');
		var items = nav.find('.menu > .menu-el');

		var wait = nav.data('wait');

        if (wait)
            return;
        nav.data('wait', true);
		
		

		if(nav.hasClass('is-opened')) {
			that.removeClass('open-nav');
		
			var tl = new TimelineMax();
			tl.pause();

			tl.fromTo(background,1.4, {
				alpha:1,
				x: 0,
				skewX : 0,
				scaleY: 1
			}, {
				alpha: 1,
				x: -$(window).width() - logo.width(),
				skewX: -45,
				scaleY: 2,
				ease: Power2.easeInOut,
				delay:0.6
			},0);

			tl.staggerFromTo(items,0.4, {
				alpha:1,
				x:0,
			}, {

				 alpha: 0

			}, 0.1, 0);
			
			tl.call(function(){

				background.css({
					opacity:'',
					transform:''
				});

				 items.css({
                    opacity: '',
                    transform: ''
                });

				
				nav.removeClass('is-opened');
				nav.data('wait', false);
				mainNavigationWrapper.removeClass('top-index');

			});

			tl.play();


		}else {
			
			mainNavigationWrapper.addClass('top-index');
			that.addClass('open-nav');

			background.css({
				opacity:0
			});

			container.css({
				display:"block"
			});

			items.css({
                opacity: 0
            });

			var tl = new TimelineMax();
			tl.pause();
			tl.fromTo(background, 1, {
				 alpha: 1,
				 x: -$(window).width() - logo.width(),
				  skewX: -45,
				  scaleY: 2
			},{
				alpha: 1,
				x: 0,
				skewX: 0,
				scaleY: 1,
				ease: Power3.easeInOut
			}, 0);

			tl.staggerFromTo(items, 1.2, {
				alpha: 0,
                x: -80,
                delay:0.4
			}, {

				 alpha: 1,
                 x: 0,
                ease: Power4.easeInOut

			}, 0.1, 0);
			
			tl.call(function() {

			 	 background.css({
                    opacity: '',
                    transform: ''
                });

			 	  items.css({
                    opacity: '',
                    transform: ''
                });

			 	  container.css({
                    display: ''
                });

			 	
			 	nav.addClass('is-opened');
			 	
			 	nav.data('wait', false);

			 });

			tl.play();

		}
	}


	// Enable ScrollMagic only for desktop, disable on touch and mobile devices
	
	if (!Modernizr.touch) {

			//Zdarzenia onMouseWheel
			if (document.addEventListener) {
			    document.addEventListener("mousewheel", MouseWheelHandler(), false);
			    document.addEventListener("DOMMouseScroll", MouseWheelHandler(), false);
			} else {
			    sq.attachEvent("onmousewheel", MouseWheelHandler());
			}

			function MouseWheelHandler() {

			    return function (e) {
			        // cross-browser wheel delta
			        var e = window.event || e;
			        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));


			        if(animationStart) {
			        	return false;
			        }


			        if (delta > 0) {
			            navLeft.trigger('click');
			        }
			        else {
			             navRight.trigger('click');
			        }
			        return false;
			    }
			}
		}





		$("body").keydown(function(e) {
		  if(e.keyCode == 37) { // left
		    navLeft.trigger('click');
		  }
		  
		  else if(e.keyCode == 39) { // right
		     navRight.trigger('click');
		  }
		});



	

	 // Animation on click main navigation
	 var viewportQuery = window.matchMedia('(min-width: 1024px)');
      viewportQuery.addListener(menuClick);

      function menuClick(viewportQuery) {
      	 if (viewportQuery.matches) {

      	 	var mainNavigationLinks = $('#menu-main li').find('a');

      	 		mainNavigationLinks.on('click', function(event){
	      	 		event.preventDefault();
	                if (event.target !== this) return;

	                var that = $(this);
					var clickedLink = that.attr('href');
					var nav = $('.main-nav-wrapper');
					var logo = $('.shape-triangle');
					var items = nav.find('.menu > .menu-el')

					var triangleShape = $('.shape-triangle');

					TweenMax.staggerTo(items, 0.5, 

						{	opacity:0, 
							y:-100, 
							ease:Back.easeIn,
							onComplete:function() {

								TweenMax.to(triangleShape, 0.5,

								 	{
								 		
								 		left: -600,
								 		autoAlpha:0,
								 		ease: Sine. easeOut,

								 		 onComplete:function() {
								 		 	TweenMax.to($('body'), 0.7,
			                                    {
			                                        autoAlpha:0, 
			                                        ease:Power4.easeInOut,
			                                        onComplete:function() {
			                                            window.location = clickedLink;
			                                        }
			                                    }
			                                )
								 		 }
								 	}
					 			)

							}
						}, 

					0.1);

      	 		});



      	 }else {

  	 		TweenMax.to($('body'),0.7,
                {
                    autoAlpha:1, 
                    ease:Power4.easeInOut
                }
            )

      	 }
      }


      var mediaQuery = window.matchMedia('(min-width: 1000px)');
      mediaQuery.addListener(callClick);

      function callClick(mediaQuery) {

      		 if (mediaQuery.matches) {

      		 	var callToAction = $('.slide-content-inner').find('.cta-btn');
      		 	var triangleShape = $('.shape-triangle');
				

      		 	callToAction.on('click',function(e){
      		 		
      	 			e.preventDefault();
      	 			var that = $(this);
      	 			var clickedLink = that.attr('href');
					

						TweenMax.to(triangleShape, 0.5,

						 	{
						 		ease:Power4.easeInOut,
						 		left:-300,
						 		autoAlpha:0,
						 		onComplete:function() {

						 			TweenMax.to($('#move-slider'), 0.5,

							 			{
							 				ease:Power4.easeInOut,
							 				left:-300,
						 					autoAlpha:0,

							 				onComplete:function() {
							 					TweenMax.to($('body'), 0.7,
					                                {
					                                    autoAlpha:0, 
					                                    ease:Power4.easeInOut,
					                                    onComplete:function() {
					                                        window.location = clickedLink;
					                                    }
					                                }
					                            )
							 				}

							 			}

						 			)
						 		}
						 		 
						 	}
						)
					});


      		 } else {
      		 	TweenMax.to($('body'),0.7,
	                {
	                    autoAlpha:1, 
	                    ease:Power4.easeInOut
	                }
            	)
      		 }

      }

      var switchQuery = window.matchMedia('(min-width: 1000px)');
      switchQuery.addListener(logoClick);

      function logoClick(switchQuery) {

      		 if (switchQuery.matches) {
      		 	var logoLink = $('.logo-link-wrapper').find('.logo-link');

      		 	logoLink.on('click',function(e){

      	 			e.preventDefault();

      	 			var that = $(this);
				    var clickedLink = that.attr('href');
				    var triangleShape = $('.shape-triangle');

      				TweenMax.to(triangleShape, 0.5,

      				 	{
      				 		ease:Power4.easeInOut,
      				 		left:-300,
      				 		autoAlpha:0,
      				 		 onComplete:function() {
      				 		 	TweenMax.to($('body'), 0.7,
                                      {
                                          autoAlpha:0, 
                                          ease:Power4.easeInOut,
                                          onComplete:function() {
                                              window.location = clickedLink;
                                          }
                                      }
                                  )
      				 		 }
      				 	}
      				)
      	 	});


      		 }else {
      		 	TweenMax.to($('body'),0.7,
	                {
	                    autoAlpha:1, 
	                    ease:Power4.easeInOut
	                }
            	)
      		 }

      }


      function filters() {
      		var filterItems = $('.filter-flex-list').find('li');

      		if($('.filter-section').length > 0) {
      				var dotcount = 0;

      			 	$('.testimonial-article.mix').each(function(){
            			$(this).attr('data-order', dotcount);
            			dotcount=dotcount+1;
       				});

       				var listsEl = $('.filter-flex-list').find('li');
       				var firstEl = listsEl.filter(':first');

       				 $('.testimonials-section').mixItUp({

			            load: {
			                sort: 'order:asc'
			            },

			            animation: {
			                effectsIn: 'fade translateY(-100%)',
			                duration: 700 /* 600 */
			            },

			            selectors: {
			                target: '.mix',
			                filter: '.filter-btn',
			                sort: '.sort-btn'
			            },

			             callbacks: {
			                onMixEnd: function(state){
			                   // resizePost();
			                }
			             }
	          		});


       				firstEl.addClass('mixitup-control-active');
      		}else {
      			return;
      		}
     }


      function fadeInContent() {
		$('.fadeIn').each(function(){
			var tween = TweenMax.from($(this),1.8, {autoAlpha:0 ,ease:Power4.easeInOut});
		
		// Build scene
			var scene = new ScrollMagic.Scene({
				reverse: false,
				offset:-200,
				triggerElement:this
			})
			.setTween(tween)
			.addTo(controller);
		});


		$('.fadeInBottom').each(function(){
			var tween = TweenMax.from($(this),1.8, 
				{	autoAlpha:0, 
					 ease: Quint.easeOut,
					y:50
				}
			);
		
		// Build scene
			var scene2 = new ScrollMagic.Scene({
				reverse: false,
				offset:-300,
				triggerElement:this
			})
			.setTween(tween)
			.addTo(controller);
		});


	}

	function addColorTest() {

			var articleClass = 	$('.testimonial-article');
			var colors = ["border-color", "no-color"];
			var counter = 0;

			articleClass.each(function(index,item){

				if(counter%colors.length === 0) {
					//Co 3 elementy przeskakuje na poczatek tablicy
					counter = 0;
				}

				// Colors od counter zwroci nazwe klasy
					$(item).addClass(colors[counter]);
					counter++;
			});

		}

function footerRotator() {
	var carouselFooter = $('.owl-carousel.footer-logos');

	if(carouselFooter.length > 0) {
		carouselFooter.owlCarousel({

			loop:true,
			dots:false,
			autoplay:true,
			smartSpeed:800,
			autoplayTimeout:3500,
			mouseDrag:false,
			freeDrag:false,
			margin:0,
			arrows:false,

			responsive:{
			        0:{
			            items:1
			        },

			         641:{
			            items:2
			        },

			        768:{
			            items:3
			        },

			        992:{
			            items:4
			        },

			        1000:{
			            items:6
			        }
    			}
		});
	}
}
	
	$(function(){
		loadComplete();
		sections.filter(':first').addClass('active-slide');
		slidesContainerWidth();
		addDataToSlide();
		createBottomPag();
		addPagBullets();
		$('.nav-toggle').off('click').on('click', runMainNav);
		menuClick(viewportQuery);
		callClick(mediaQuery);
		logoClick(switchQuery);
		filters();
		fadeInContent();
		addColorTest();
		videoFullHd();
		footerRotator();
	});


	 function onPopStateHandler(event) {
        var state = event.state;
        if (state) {
            wasUsedBackButton = true;
            findHash();

            var nav = $('.main-nav-wrapper');
			var logo = $('.shape-triangle');
			var items = nav.find('.menu > .menu-el')
			var triangleShape = $('.shape-triangle');

					TweenMax.staggerTo(items, 0.5, 

						{	opacity:1, 
							y:0, 
							ease:Back.easeIn,
							onComplete:function() {

								TweenMax.to(triangleShape, 0.5,

								 	{
								 		ease:Power4.easeInOut,
								 		left:0,
								 		autoAlpha:1,
								 		 onComplete:function() {
								 		 	TweenMax.to($('body'), 0.7,
			                                    {
			                                        autoAlpha:1, 
			                                        ease:Power4.easeInOut
			                                    }
			                                )
								 		 }
								 	}
					 			)

							}
						}
					)

					TweenMax.to($('#move-slider'), 0.5,
						{
							autoAlpha:1,
							left:0,
                    		ease: SlowMo.easeIn
						}
					)

            TweenMax.to($('body'),0.4,
                {
                   autoAlpha:1,
                    ease: SlowMo.easeIn
                }
            )
        }
    }

    $(window).unload(function(){
        TweenMax.to($('body'),0.4,
            {
                autoAlpha:1,
                ease: SlowMo.easeIn
            }
        )
    });


    function changeTopHeader() {
    	var logoTriangle = $('.shape-triangle');

    	if($(window).scrollTop() > 5) {
    		logoTriangle.addClass('scroll-header');
    	}else {
    		logoTriangle.removeClass('scroll-header');
    	}
    }


	$(window).on('scroll', function(){
		changeTopHeader();
	});


	$(window).on('load',function(){
		introSlide();
		

		var hash = window.location.hash.replace("#","");

		var hash = window.location.hash.replace("#","");

         if(hash==='') {
            // Wywolanie bez hasha
            $('#home-pagination li span').filter(":first").trigger("click");


        }else {
            findHash();

            // Removes hash from browsing history
            //$('.close-panel-team').trigger('click');
        }


		setTimeout(function () {
            $('#home-pagination li').find('span').on('click', sliderRotate);
            findHash();
        }, 100);

        setTimeout(function () {
            window.onpopstate = onPopStateHandler;
        }, 1000);
	});

	$(window).on('resize',function(){
		slidesContainerWidth();

		var allDiamonds = $('#home-pagination li').children();
		allDiamonds.filter('.active-diamond').trigger('click');
		
		
		setTimeout(function(){
			videoFullHd();
		},100);
	});

})(jQuery);
