(function($){
	var preloader = $('#preloader');
	var imagesToLoad = $('.bcg').length;
	var loadingProgress = 0; //timeline progress - starts at 0
	var loadedCount = 0; //current number of images loaded

	var pageNumber = 1; //Posts will start from first page
   	var content = $('#news-section');
    var loading = true;
    var win = $(window);
    var logoTriangle = $('.shape-triangle');


   

	function loadComplete() {

		// preloader out
		var preloaderOutTl = new TimelineMax();
		var shapeNavigation = $('.shape-triangle');

		preloaderOutTl
			//.to($('.progress'), 0.3, {autoAlpha: 0, ease:Back.easeIn})
			//.to($('.txt-perc'), 0.3, {autoAlpha: 0, ease:Back.easeIn}, 0.1)

			.to($('.intro-logo-container'),0.5, {autoAlpha: 0, ease:Back.easeIn})
			.set($('body'), {className: '-=is-loading'})
			//.to($('.main-header'),0.7, {autoAlpha: 1,top: 0, ease:Power4.easeInOut,delay:1.5})
			//.set($('#intro'), {className: '+=is-loaded'})
			.to($('#preloader'), 0.7, {autoAlpha: 0, ease:Power4.easeInOut})
			.set($('#preloader'), {className: '+=is-hidden'})
			//.from($('#intro p'), 0.7, {autoAlpha: 0, ease:Power1.easeOut}, '+=0.1')

			/*.to($('#cases-slider'),0.4,

			{
				autoAlpha:1,
				ease: Power1.easeInOut
			},0)*/


			.to(shapeNavigation,0.2,

				{
					autoAlpha:1,
					left:0,
					ease:Power4.easeInOut
				}

			)


			return preloaderOutTl;
	}



	// Main navigation functinality
	runMainNav = function() {
		var that = $(this);
		var nav = $('.main-nav-wrapper');
		var background = nav.find('.background');
		var container = nav.find('.nav-container');
		var logo = $('.shape-triangle');
		var mainNavigationWrapper = $('.navigation-hidden-switcher');
		var items = nav.find('.menu > .menu-el');

		var wait = nav.data('wait');

        if (wait)
            return;
        nav.data('wait', true);
		
		

		if(nav.hasClass('is-opened')) {
			that.removeClass('open-nav');
		
			var tl = new TimelineMax();
			tl.pause();

			tl.fromTo(background,1.4, {
				alpha:1,
				x: 0,
				skewX : 0,
				scaleY: 1
			}, {
				alpha: 1,
				x: -$(window).width() - logo.width(),
				skewX: -45,
				scaleY: 2,
				ease: Power2.easeInOut,
				delay:0.6
			},0);

			tl.staggerFromTo(items,0.4, {
				alpha:1,
				x:0,
			}, {

				 alpha: 0

			}, 0.1, 0);
			
			tl.call(function(){

				background.css({
					opacity:'',
					transform:''
				});

				 items.css({
                    opacity: '',
                    transform: ''
                });

				
				nav.removeClass('is-opened');
				nav.data('wait', false);
				mainNavigationWrapper.removeClass('top-index');

			});

			tl.play();


		}else {
			
			mainNavigationWrapper.addClass('top-index');
			that.addClass('open-nav');

			background.css({
				opacity:0
			});

			container.css({
				display:"block"
			});

			items.css({
                opacity: 0
            });

			var tl = new TimelineMax();
			tl.pause();
			tl.fromTo(background, 1, {
				 alpha: 1,
				 x: -$(window).width() - logo.width(),
				  skewX: -45,
				  scaleY: 2
			},{
				alpha: 1,
				x: 0,
				skewX: 0,
				scaleY: 1,
				ease: Power3.easeInOut
			}, 0);

			tl.staggerFromTo(items, 1.2, {
				alpha: 0,
                x: -80,
                delay:0.4
			}, {

				 alpha: 1,
                 x: 0,
                ease: Power4.easeInOut

			}, 0.1, 0);
			
			tl.call(function() {

			 	 background.css({
                    opacity: '',
                    transform: ''
                });

			 	  items.css({
                    opacity: '',
                    transform: ''
                });

			 	  container.css({
                    display: ''
                });

			 	
			 	nav.addClass('is-opened');
			 	
			 	nav.data('wait', false);

			 });

			tl.play();

		}
	}


	function mixItUp() {
       	var dotcount = 0;
       	var listsEl = $('.filter-flex-list').find('li'); 
       	var allArticles =  $('#news-section').find('.blog-article');
       	var listsEl = $('.filter-flex-list').find('li');
		var firstEl = listsEl.filter(':first');

		firstEl.addClass('mixitup-control-active');

		allTextxsInside = listsEl.find('span');
		var catText = allTextxsInside.text();

		//allArticles.addClass("filter-active");
		selectedClick =  $('.filter-flex-list li').not(":eq(0)");



		selectedClick.on('click',function(e){
			e.preventDefault();
			
			var that = $(this);
			var currentText = that.children('span').text();
			$('.top-badge-text').text(currentText);


			$('.filter-flex-list').find('li').removeClass('mixitup-control-active');
			that.addClass('mixitup-control-active');


			var targetData = that.data('filter');
			var currentTarget = allArticles.filter("." + targetData);

			currentTarget.siblings().addClass("filter-hide");
			currentTarget.removeClass("filter-hide");
			currentTarget.addClass("filter-active");

			return false;

		});

		firstEl.on('click',function(e){
			e.preventDefault();
			
			$('.filter-flex-list').find('li').removeClass('mixitup-control-active');
			var that = $(this);
			var currentText = that.children('span').text();
			$('.top-badge-text').text(currentText);

			that.addClass('mixitup-control-active');


			setTimeout(function(){

				allArticles.removeClass("filter-hide");
				allArticles.removeClass("filter-active");

			},300);

			return false;
		});

		var texts = $('.short-blog-content').find('p');

		$('.switcher-text').on('click',function(){
			texts.toggleClass('show-text');
		});
		

    }





   function mixPlug() {
 
	$('.filter-flex-list li').first().click();

	$('.blog-article').each(function(i){

				var that = $(this);
				that.attr('data-my-order',(i+1));

			});

	
	//firstEl.addClass('mixitup-control-active');

 		var mixer = mixitup(content, {
			    load: {
			         sort: false
			    },

			    animation: {
	                enable:false,
					duration: 300,
					effectsOut: 'fade translateY(-100%)'
	            },

	            selectors: {
	                target: '.mix',
	               control: '.filter'
	            },

	            callbacks: {
        			onMixStart: function(state, futureState) {
             				animation:{
             					enable:true

             				}
        				}
    				}
			});

              

    }


     
    function load_posts() {
        $.ajax({

            url:baseUrl + "/loop-ajax.php",
            type:"GET",
            dataType:"html",
            data: {GETposts : postsCount, GETpage : pageNumber},

            beforeSend:function() {
                content.append('<div style="text-align:center; display:block;" id="loading"><img src="' + baseUrl + '/gfx/ajax-loader.gif"></div>');
            },

            success: function(data) {
                    var tekst = data.replace(/\s+/g, ''); // Aby uniknac wstawiania spacji tworzymy te zmienna
                    if (tekst.length) {

                        content.append(data);
                        loading = false; //Zakonczylo sie ladowanie postow
                        $("#loading").remove();

                    } else {
                        $("#loading").remove();
                      
                        content.append('<div style="text-align:center;">New stories are coming soon!</div>')
                    }

                   

                     //mixPlug();
                    mixItUp();
                },

                error: function() {
                    console.log('Something went wrong with Ajax!');

            }

        });

    }



     function changeTopHeader() {
    	var logoTriangle = $('.shape-triangle');

    	if($(window).scrollTop() > 5) {
    		logoTriangle.addClass('scroll-header');
    	}else {
    		logoTriangle.removeClass('scroll-header');
    	}
    }




    win.on('scroll', function(){
        if ( !loading && ( win.scrollTop() + win.height() ) > ( content.scrollTop() + content.height() + content.offset().top ) ) {
            loading = true;
            pageNumber++;
			load_posts();
			//var mixer = mixitup(content);
			//mixer.destroy();
			var allArticles =  $('#news-section').find('.blog-article');
			var listsEl = $('.filter-flex-list').find('li');
			var firstEl = listsEl.filter(':first');
			allArticles.removeClass("filter-hide");
			allArticles.removeClass("filter-active");
			$('.filter-flex-list li').removeClass('mixitup-control-active');
			firstEl.addClass('mixitup-control-active');
        };


        	

        changeTopHeader();

     });



	//Calling functions inside document ready
	$(function(){
		// Calling navigation function
		$('.nav-toggle').off('click').on('click', runMainNav);

		loadComplete();

	});

	$(window).on('load', function(){
        load_posts();

        //mixItUp();
    });



})(jQuery);