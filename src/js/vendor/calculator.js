(function($){

    var NumEmployeesFormat = "{NUM} employees";
    var WorkingHoursFormat = "{NUM} Hours";
    var PayRatePerHourFormat = "£{NUM} per hour";

    var NumEmployeesMin = 1;
    var NumEmployeesmax = 10000;
    var WorkingHoursMin = 5;
    var WorkingHoursMax = 48;
    var PayRatePerHourMin = 5;
    var PayRatePerHourMax = 120;
    var currencySymbol = "£";

    $(document).ready(function () {

    // num employees
    $("#slider-employees").slider({
        range: "min",
        value: $("#calcNumEmployees").val().replace(/[^\d.-]/g, ''),
        min: NumEmployeesMin,
        max: NumEmployeesmax,
        slide: function (event, ui) {
            var newVal = ui.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            newVal = NumEmployeesFormat.replace("{NUM}", newVal);
            $("#txtEmployees").text(newVal);
            $("#calcNumEmployees").val(newVal);
            updateResults();

            if ($("#txtEmployees").is(":hidden")) {
                $("#txtEmployees").slideDown("slow", function () {
                    displayResults();
                });
            }
        }
    });
    
    $("#calcNumEmployees").on('change keyup paste mouseup', function () {
        var value = $("#calcNumEmployees").val();
        value = value.replace(/[^\d.-]/g, '');
        $("#slider-employees").slider('option', 'value', value);
    });

    $("#calcNumEmployees").on('blur', function () {
        var value = $("#calcNumEmployees").val();

        if ((value == "") && (old_calcNumEmployees != ""))
        {
            $("#calcNumEmployees").val(old_calcNumEmployees);
            value = old_calcNumEmployees.replace(/[^\d.-]/g, '');
        }
        else
        {
            value = value.replace(/[^\d.-]/g, '').toString();
            if (value == "")
                value = "1";
            if (value <= 0)
                value = "1";
            var newVal = value.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' employees';
            $("#calcNumEmployees").val(newVal);
        }
        $("#slider-employees").slider('option', 'value', value);
    });
    
    var old_calcNumEmployees = "";
    $("#calcNumEmployees").on('focus', function () {
        old_calcNumEmployees = $("#calcNumEmployees").val();
        $("#calcNumEmployees").val("");
    });


    // working hours
    $("#slider-workinghours").slider({
        range: "min",
        value: $("#calcWorkingHours").val().replace(/[^\d.-]/g, ''),
        min: WorkingHoursMin,
        max: WorkingHoursMax,
        step: 0.5,
        slide: function (event, ui) {
            var newVal = ui.value;
            newVal = WorkingHoursFormat.replace("{NUM}", newVal)
            $("#txtWorkingHours").text(newVal);
            $('#calcWorkingHours').val(newVal);
            updateResults();

            if ($("#txtWorkingHours").is(":hidden")) {
                $("#txtWorkingHours").slideDown("slow", function () {
                    displayResults();
                });
            }
        }
    });

    $("#calcWorkingHours").on('change keyup paste mouseup', function () {
        var value = $("#calcWorkingHours").val();
        value = value.replace(/[^\d.-]/g, '');
        $("#slider-workinghours").slider('option', 'value', value);
    });

    $("#calcWorkingHours").on('blur', function () {
        var value = $("#calcWorkingHours").val();

        if ((value == "") && (old_calcWorkingHours != "")) {
            $("#calcWorkingHours").val(old_calcWorkingHours);
            value = old_calcWorkingHours.replace(/[^\d.-]/g, '').toString();
        }
        else {
            value = value.replace(/[^\d.-]/g, '').toString();
            if (value == "")
                value = "1";
            if (value <= 0)
                value = 1;
            var newVal = value + ' hours';
            $("#calcWorkingHours").val(newVal);
        }
        $("#slider-workinghours").slider('option', 'value', value);
    });

    var old_calcWorkingHours = "";
    $("#calcWorkingHours").on('focus', function () {
        old_calcWorkingHours = $("#calcWorkingHours").val();
        $("#calcWorkingHours").val("");
    });


    // Pay rate
    $("#slider-payrate").slider({
        range: "min",
        value: $("#calcPayRatePerHour").val().replace(/[^\d.-]/g, ''),
        min: PayRatePerHourMin,
        max: PayRatePerHourMax,
        step: 0.5,
        slide: function (event, ui) {
            var newVal = ui.value;
            newVal = PayRatePerHourFormat.replace("{NUM}", newVal)
            $("#txtPayrate").text(newVal);
            $('#calcPayRatePerHour').val(newVal);
            updateResults();

            if ($("#txtPayrate").is(":hidden")) {
                $("#txtPayrate").slideDown("slow", function () {
                    displayResults();
                });
            }
        }
    });

    $("#calcPayRatePerHour").on('change keyup paste mouseup', function () {
        var value = $("#calcPayRatePerHour").val();
        value = value.replace(/[^\d.-]/g, '');
        $("#slider-payrate").slider('option', 'value', value);
    });

    $("#calcPayRatePerHour").on('blur', function () {
        var value = $("#calcPayRatePerHour").val();

        if ((value == "") && (calcPayRatePerHour != "")) {
            $("#calcPayRatePerHour").val(old_calcPayRatePerHour);
            value = old_calcPayRatePerHour.replace(/[^\d.-]/g, '').toString();
        }
        else {
            value = value.replace(/[^\d.-]/g, '').toString();
            if (value == "")
                value = "1";
            if (value <= 0)
                value = 1;
            var newVal = currencySymbol + value + ' per hour';
            $("#calcPayRatePerHour").val(newVal);
        }
        $("#slider-payrate").slider('option', 'value', value);
    });

    var old_calcPayRatePerHour = "";
    $("#calcPayRatePerHour").on('focus', function () {
        old_calcPayRatePerHour = $("#calcPayRatePerHour").val();
        $("#calcPayRatePerHour").val("");
    });


    //initialise textbox values
    // num employees
    var newVal = $("#slider-employees").slider('value').toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $("#calcNumEmployees").val(NumEmployeesFormat.replace("{NUM}", newVal));
    // working hours
    newVal = $("#slider-workinghours").slider('value');
    $("#calcWorkingHours").val(WorkingHoursFormat .replace("{NUM}", newVal));
    // payrate
    newVal = $("#slider-payrate").slider('value');
    $("#calcPayRatePerHour").val(PayRatePerHourFormat.replace("{NUM}", newVal));
    updateResults();
});

function updateResults() {
    //==============================
    //  Capture form values
    //==============================
    var numberOfEmployees = $('#calcNumEmployees').val().replace(/[^\d.-]/g, ''); //.replace(" employees", "").replace(",", "");
    var workingHoursPerWeek = $('#calcWorkingHours').val().replace(/[^\d.-]/g, ''); //.replace(" hours", "");
    var payRatePerHour = $('#calcPayRatePerHour').val().replace(/[^\d.-]/g, ''); //.replace("£", "").replace(" per hour", "");

    var totalHoursPerWeek = numberOfEmployees * workingHoursPerWeek;
    var totalWeeklyPay = totalHoursPerWeek * payRatePerHour;

    //====================
    //  1. Overpayment
    //====================
    var hoursGainedWeekly = totalHoursPerWeek * 0.01;
    var overpaymentSaving = hoursGainedWeekly * payRatePerHour;

    $('#hoursGainedWeekly').text(hoursGainedWeekly.toFixed(2));
    $('#overpaymentSavingsPerWeek').text(currencySymbol + overpaymentSaving.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

    //====================
    //  2. Human error
    //====================
    var rateOfHumanErrorPerWeek = 0.005; //0.5% CHECK
    var humanErrorSaving = totalWeeklyPay * rateOfHumanErrorPerWeek;

    $('#humanErrorSavingsPerWeek').text(currencySymbol + humanErrorSaving.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

    //==============================
    //  3. Administration costs
    //==============================
    var calculationEntry = (numberOfEmployees * 5) / 60;
    var adminSaving = calculationEntry * payRatePerHour;

    $('#calculationAndEntry').text(calculationEntry.toFixed(2));
    $('#adminSavingsPerWeek').text(currencySymbol + adminSaving.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

    //==============================
    //   4. Overall savings
    //==============================
    var totalSavingPerWeek = overpaymentSaving + humanErrorSaving + adminSaving;
    var totalSavingPerYear = totalSavingPerWeek * 52;

    $('#totalSavingPerWeek').text(currencySymbol + totalSavingPerWeek.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    $('#totalSavingPerYear').text(currencySymbol + totalSavingPerYear.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
}

function displayResults() {
    // Toggle results display when 3 inputs are defined
    if ($("#txtEmployees").is(":visible") && $("#txtWorkingHours").is(":visible") && $("#txtPayrate").is(":visible")) {
        $("#results").slideDown("slow");
    }
}

//go to calculator anchor
/*$("#scroll-calculator").click(function () {    
    if ($(".calculator")[0]) {        
        $('html, body').animate({
            scrollTop: $(".calculator").offset().top
        }, 2000);
    }
    else
    {
        //direct to home page if current page has no calculator
        window.location.href = "/home#calculator";
    }
});*/


})(jQuery);