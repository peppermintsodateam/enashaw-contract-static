(function($){

	$(document).ready( function() {

  
	//Google Maps JS
	//Set Map
	function initialize() {
			var myLatlng = new google.maps.LatLng(53.418145,-2.739922);
			//var imagePath = 'http://m.schuepfen.ch/icons/helveticons/black/60/Pin-location.png'
			//var imagePath = "http://peppermintdigital.com/green-walk/gfx/map-marker.png"



			var mapOptions = {
				zoom: 16,
				center: myLatlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				scrollwheel: false,
        		navigationControl: false,
        		mapTypeControl: false,
        		scaleControl: false,
        		draggable: true,

			};

		var map = new google.maps.Map(document.getElementById('map'), mapOptions);
		//Callout Content
		var contentString = 'Ena Shaw Ltd - Factory, Showroom and Contracts';
		//Set window width + content
		var infowindow = new google.maps.InfoWindow({
			content: contentString,
			maxWidth: 500
		});

		//Add Marker
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			icon:"http://enashaw.wordpress.peppermintdigital.com/ena-wordpress2017/wp-content/uploads/2017/10/pin.png",
			title: 'Ena Shaw Pin'
		});



		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
		});


		/*map.init({
			location: "53.378489, -2.364373",
			mapMarker:"/wp-content/themes/theme-green-walk/gfx/map-marker.png"
		});*/

		//Resize Function
		google.maps.event.addDomListener(window, "resize", function() {
			var center = map.getCenter();
			google.maps.event.trigger(map, "resize");
			map.setCenter(center);
			google.maps.event.trigger(map, "resize");
			
		});
	}
	
	google.maps.event.addDomListener(window, 'load', initialize);

});

})(jQuery);